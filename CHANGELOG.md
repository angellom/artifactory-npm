# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.2

- patch: Adding git in docker image

## 0.3.1

- patch: JFrog CLI version to 1.24.1

## 0.3.0

- minor: checking status of each executed command.

## 0.2.4

- patch: updated readme.

## 0.2.3

- patch: Added build-add-git (bag) command 

## 0.2.2

- patch: Updated links

## 0.2.1

- patch: Updated support link

## 0.2.0

- minor: Minor Release

## 0.1.0

- minor: Initial release

