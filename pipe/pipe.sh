#!/bin/bash
#
# artifactory-npm pipe
#
# Required globals:
#   ARTIFACTORY_URL
#   ARTIFACTORY_USER
#   ARTIFACTORY_PASSWORD
#   NPM_COMMAND
#   NPM_SOURCE_REPO
#   NPM_TARGET_REPO
#
# Optional globals:
#   EXTRA_ARGS
#   JFROG_CLI_TEMP_DIR
#   JFROG_CLI_HOME_DIR
#   COLLECT_ENV
#   COLLECT_GIT_INFO
#   COLLECT_BUILD_INFO
#   BUILD_NAME
#   EXTRA_BAG_ARGS
#

source "$(dirname "$0")/common.sh"

## Enable debug mode.
DEBUG_ARGS=
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    DEBUG_ARGS="--verbose"
    export JFROG_CLI_LOG_LEVEL="DEBUG"
  fi
}

info "Starting pipe execution..."

# required parameters
ARTIFACTORY_URL=${ARTIFACTORY_URL:?'ARTIFACTORY_URL variable missing.'}
ARTIFACTORY_USER=${ARTIFACTORY_USER:?'ARTIFACTORY_USER variable missing.'}
ARTIFACTORY_PASSWORD=${ARTIFACTORY_PASSWORD:?'ARTIFACTORY_PASSWORD variable missing.'}
NPM_COMMAND=${NPM_COMMAND:?'NPM_COMMAND variable missing.'}
if [[ "${NPM_COMMAND}" == "install" ]]; then
  NPM_SOURCE_REPO=${NPM_SOURCE_REPO:?'NPM_SOURCE_REPO variable missing.'}
fi
if [[ "${NPM_COMMAND}" == "publish" ]]; then
NPM_TARGET_REPO=${NPM_TARGET_REPO:?'NPM_TARGET_REPO variable missing.'}
fi

# optional parameters
BUILD_NAME=${BUILD_NAME:=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH}
JFROG_CLI_TEMP_DIR=${JFROG_CLI_TEMP_DIR:="."}
JFROG_CLI_HOME_DIR=${JFROG_CLI_HOME_DIR:="."}
COLLECT_ENV=${COLLECT_ENV:="true"}
COLLECT_GIT_INFO=${COLLECT_GIT_INFO:="true"}
COLLECT_BUILD_INFO=${COLLECT_BUILD_INFO:="true"}
EXTRA_ARGS=${EXTRA_ARGS:=""}
EXTRA_BAG_ARGS=${EXTRA_BAG_ARGS:=""}
DEBUG=${DEBUG:="false"}

# Set the environment variable
export JFROG_CLI_TEMP_DIR=$JFROG_CLI_TEMP_DIR
export JFROG_CLI_HOME_DIR=$JFROG_CLI_HOME_DIR
export BUILD_URL="https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}"

# Getting package.json configuration
package_json=$(cat ./package.json)
debug "package.json file: ${package_json}"

package_name=$(echo "${package_json}" | jq -r '.name')
package_version=$(echo "${package_json}" | jq -r '.version')

debug "build name is ${BUILD_NAME}"
debug "build number is ${BITBUCKET_BUILD_NUMBER}"

check_status() {
if [[ "${status}" -eq 0 ]]; then
  success "Package \"$package_name@$package_version\" published successfully."
else
  fail "Failed to publish package \"$package_name@$package_version\"."
fi
}

# Configure Artifactory instance with JFrog CLI
run jfrog rt config --url=$ARTIFACTORY_URL --user=$ARTIFACTORY_USER --password=$ARTIFACTORY_PASSWORD --interactive=false
check_status

# Run NPM install command to pull dependency from Artifactory NPM registry
if [[ "${NPM_COMMAND}" == "install" ]]; then
run jfrog rt npmi $NPM_SOURCE_REPO --build-name=$BUILD_NAME --build-number=$BITBUCKET_BUILD_NUMBER $EXTRA_ARGS
check_status
fi

# Run NPM publish command to publish created NPM package to Artifactory NPM registry
if [[ "${NPM_COMMAND}" == "publish" ]]; then
run jfrog rt npmp $NPM_TARGET_REPO --build-name=$BUILD_NAME --build-number=$BITBUCKET_BUILD_NUMBER $EXTRA_ARGS
check_status
fi

# Capture environment variables for build information
if [[ "${COLLECT_ENV}" == "true" ]]; then
   info "Capturing environment variables"
   run jfrog rt bce $BUILD_NAME $BITBUCKET_BUILD_NUMBER
   check_status
fi

# Collecting Information from Git
if [[ "${COLLECT_GIT_INFO}" == "true" ]]; then
   info "Collecting Information from Git"
   run jfrog rt bag $BUILD_NAME $BITBUCKET_BUILD_NUMBER $EXTRA_BAG_ARGS
   check_status
fi

# Publish build information to Artifactory
if [[ "${COLLECT_BUILD_INFO}" == "true" ]]; then
   info "Capturing build information"
   run jfrog rt bp $BUILD_NAME $BITBUCKET_BUILD_NUMBER --build-url="${BUILD_URL}"
   check_status
fi

