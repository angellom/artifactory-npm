# Bitbucket Pipelines Pipe: Artifactory Npm

This pipe triggers an npm build to deploying npm packages to [JFrog Artifactory](https://jfrog.com/artifactory/?utm_source=BitbucketPipes&utm_medium=UX).

By default, this pipe will also capture build-info and publish it to Artifactory as metadata associated with the built artifacts.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: JfrogDev/artifactory-npm:0.3.2
  variables:
    ARTIFACTORY_URL: '<string>'
    ARTIFACTORY_USER: '<string>'
    ARTIFACTORY_PASSWORD: '<string>'
    NPM_COMMAND: 'publish'
    NPM_TARGET_REPO: '<string>'
    NPM_SOURCE_REPO: '<string>'
    # BUILD_NAME: '<string>' # Optional.
    # JFROG_CLI_TEMP_DIR: '<string>' # Optional.
    # JFROG_CLI_HOME_DIR: '<string>' # Optional.
    # COLLECT_ENV: '<boolean>' # Optional.
    # COLLECT_GIT_INFO: '<boolean>' # Optional.
    # COLLECT_BUILD_INFO: '<boolean>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # EXTRA_BAG_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable        | Usage                                     |
| --------------  | ----------------------------------------- |
| ARTIFACTORY_URL (\*)  | The JFrog Artifactory URL . |
| ARTIFACTORY_USER (\*)  | Artifactory User with permission to create and access artifacts. |
| ARTIFACTORY_PASSWORD (\*)  | Password for Artifactory User. |
| NPM_COMMAND (\*)  | NPM command to perform (install/publish). Default: `publish` |
| NPM_SOURCE_REPO (\*)  | Artifactory NPM registry to download and push artifacts. |
| NPM_TARGET_REPO (\*)  | Artifactory NPM registry to download and push artifacts. |
| BUILD_NAME | Build Name. Default: `$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH` |
| JFROG_CLI_TEMP_DIR  | Specifies the JFrog CLI temp directory. Default: `.` |
| JFROG_CLI_HOME_DIR  | Specifies the JFrog CLI home directory. Default: `.` |
| COLLECT_ENV  | This flag is used to collect environment variables and attach them to a build. Default: `true` |
| COLLECT_GIT_INFO  | This flag is used to [collects the Git](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory#CLIforJFrogArtifactory-BuildIntegration-AddingGitInformation) revision and URL from the local .git directory and adds it to the build-info. Default: `true` |
| COLLECT_BUILD_INFO  | This flag is used to publish build info to Artifactory. Default: `true` |
| EXTRA_ARGS      | Extra arguments to be passed to the JFrog CLI command (see [JFrog CLI docs](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory) for more details). Defaults to unset. |
| EXTRA_BAG_ARGS  | Extra arguments to be passed to the build-add-git (bag) JFrog CLI command (see [JFrog CLI docs](https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory#CLIforJFrogArtifactory-BuildIntegration-AddingGitInformation) for more details). Defaults to unset. |
| DEBUG           | Set to _true_ to output additional debug information. Default: `false`. |

_(\*) = required variable._

## Details

The pipe publishes the contents of the current build directory. It will honor any `.npmignore` and `.gitignore` files, if present, to exclude contents as described in
the [npm Developer Guide](https://docs.npmjs.com/misc/developers#keeping-files-out-of-your-package).

## Prerequisites
[JFrog Artifactory](https://jfrog.com/artifactory/?utm_source=BitbucketPipes&utm_medium=UX) details are necessary to use this pipe.

- Add the Credentials as a [secured environment variable](https://confluence.atlassian.com/x/0CVbLw#Environmentvariables-Securedvariables) in Bitbucket Pipelines.
- [Artifactory NPM Registry](https://www.jfrog.com/confluence/display/RTF/Npm+Registry) is required.

## Examples

### Basic example 
* Install Npm packages from [Artifactory NPM Registry](https://www.jfrog.com/confluence/display/RTF/Npm+Registry). 

```yaml
script:
  - pipe: JfrogDev/artifactory-npm-install:0.3.2
    variables:
      ARTIFACTORY_URL: '<string>'
      ARTIFACTORY_USER: ${ARTIFACTORY_USER}
      ARTIFACTORY_PASSWORD: ${ARTIFACTORY_PASSWORD}
      NPM_COMMAND: 'install'
      NPM_SOURCE_REPO: 'npm-virtual'
```

* Publishes a package from the current directory to [Artifactory NPM Registry](https://www.jfrog.com/confluence/display/RTF/Npm+Registry).

```yaml
script:
  - pipe: JfrogDev/artifactory-npm:0.3.2
    variables:
      ARTIFACTORY_URL: '<string>'
      ARTIFACTORY_USER: ${ARTIFACTORY_USER}
      ARTIFACTORY_PASSWORD: ${ARTIFACTORY_PASSWORD}
      NPM_COMMAND: 'publish'
      NPM_TARGET_REPO: 'npm-local'
```
Note: Current directory should contain the `package.json` file.

### Advanced example 
* Install Npm packages from [Artifactory NPM Registry](https://www.jfrog.com/confluence/display/RTF/Npm+Registry) without capturing environment variables in build information.

```yaml
script:
  - pipe: JfrogDev/artifactory-npm:0.3.2
    variables:
      ARTIFACTORY_URL: '<string>'
      ARTIFACTORY_USER: ${ARTIFACTORY_USER}
      ARTIFACTORY_PASSWORD: ${ARTIFACTORY_PASSWORD}
      NPM_COMMAND: 'install'
      NPM_SOURCE_REPO: 'npm-virtual'
      COLLECT_ENV: 'false'
```

* Publishes a package to [Artifactory NPM Registry](https://www.jfrog.com/confluence/display/RTF/Npm+Registry) without capturing environment variables in build information.

```yaml
script:
  - pipe: JfrogDev/artifactory-npm:0.3.2
    variables:
      ARTIFACTORY_URL: '<string>'
      ARTIFACTORY_USER: ${ARTIFACTORY_USER}
      ARTIFACTORY_PASSWORD: ${ARTIFACTORY_PASSWORD}
      NPM_COMMAND: 'publish'
      NPM_TARGET_REPO: 'npm-local'
      COLLECT_ENV: 'false'
```
Note: Current directory should contain the `package.json` file.

_package.json_ file example:
```json
{
  "name": "package",
  "version": "1.0.11",
  "description": "Description"
}
```

## Support
If you'd like help with this pipe, or you have an issue or feature request, [let us know on](https://bitbucket.org/JfrogDev/artifactory-npm).

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
